#!/usr/bin/python3

import os
from dotenv import load_dotenv
from pushbullet import Pushbullet
import json, urllib

load_dotenv()

PB_API_KEY = os.getenv('PB_API_KEY')
OWM_API_KEY = os.getenv('OWM_API_KEY')
CITY = os.getenv('CITY')
NOTIF_TITLE = os.getenv('NOTIF_TITLE')
PB_DEVICE_ID = os.getenv('PB_DEVICE_ID'); # Send to specific device (run print(pb.devices) to get key)
OWM_API_BASE_URL = "https://api.openweathermap.org/data/3.0/"


def getWeather():
    content = ""
    
    weather_gps = json.loads(urllib.request.urlopen("https://api.openweathermap.org/geo/1.0/direct?q="+urllib.parse.quote_plus(CITY.encode("utf8"))+"&limit=1&appid="+OWM_API_KEY).read())

    if 'lon' in weather_gps[0]:
        gps_lon = str(weather_gps[0]['lon'])
        gps_lat = str(weather_gps[0]['lat'])

        weather = json.loads(urllib.request.urlopen(OWM_API_BASE_URL+"onecall?appid="+OWM_API_KEY+"&lat="+gps_lat+"&lon="+gps_lon+"&units=metric&lang=fr&exclude=hourly").read())
        
        if 'current' in weather:
            current_temp = str(weather['current']['temp'])
            current_temp_feels = str(weather['current']['feels_like'])
            weather_desc = str(weather['current']['weather'][0]['description'])
            day_max_temp =  str(weather['daily'][0]['temp']['max'])
            
            content = "Il fait actuellement "+current_temp+"°C ("+current_temp_feels+"°C ressenti) à "+CITY+"."
            content += "\n"
            content += "Le temps est "+weather_desc+"."
            content += "\n"
            content += "La maximale sera de "+day_max_temp+"°C."
    
    return content;


def SendWeatherWithPushbullet():

    pb = Pushbullet(PB_API_KEY)
    # print(pb.devices)

    weather = getWeather()

    if PB_DEVICE_ID != "":
       device = pb.devices[PB_DEVICE_ID]
       push = pb.push_note(NOTIF_TITLE, weather, device=device)
    else:
       push = pb.push_note(NOTIF_TITLE, weather)




if __name__ == '__main__':
    SendWeatherWithPushbullet()
