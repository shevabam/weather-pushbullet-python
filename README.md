
# Weather To Pushbullet

Script Python qui envoie une notification Pushbullet contenant la météo du jour d'une ville en particulier.

Il est nécessaire d'avoir des clés API OpenWeatherMap et Pushbullet pour que le script fonctionne (voir ci-dessous).


## API OpenWeatherMap

Le script utilise l'API d'OpenWeatherMap qui permet d'obtenir la météo du jour gratuitement. Pour pouvoir accéder à l'API, veuillez suivez ces étapes : 

* Créez un [compte sur OpenWeatherMap](https://home.openweathermap.org/users/sign_up), c'est gratuit
* Dans votre compte, cliquez sur **My API keys**
* Créez une clé en renseignant un nom et en cliquant sur le bouton **Generate**
* Récupérez vos identifiants la clé API


## Pushbullet

Si ce n'est pas déjà le cas, créez un compte [Pushbullet](https://www.pushbullet.com/). Créez ensuite un **Access Token** depuis le menu **Settings > Account**.


## Installation

Installez les dépendances via : 

    pip install -r requirements.txt

Copiez le fichier `.env.example` en `.env` et ouvrez-le pour y inscrire les différentes clés : 

* `PB_API_KEY` : **access token** de votre compte Pushbullet
* `PB_DEVICE_ID` : permet d'envoyer la notification sur un périphérique dédié. Par défaut, la notification est envoyée sur tous les périphériques liés à Pushbullet.
* `OWM_API_KEY` : clé API de votre compte OpenWeatherMap
* `CITY` : nom de la ville
* `NOTIF_TITLE` : titre de la notification Pushbullet


## Utilisation

Il suffit simplement d'exécuter le script : 

    python WeatherToPushbullet.py

Une nofication Pushbullet est alors envoyée contenant la météo du jour :

    Météo du jour
    Il fait actuellement 14.34°C (12.8°C ressenti) à PARIS.
    Le temps est peu nuageux.
    La maximale sera de 14.38°C.


